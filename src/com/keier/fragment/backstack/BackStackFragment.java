package com.keier.fragment.backstack;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

public class BackStackFragment extends Fragment {
	/** 根列表*/
	public  static List<String> mRootTagList = new LinkedList<String>();
	public  static HashMap<String, Fragment> mRootFragmentMap = new HashMap<String, Fragment>();
	
	/** 分支列表*/
	private  static List<String> mBranchTagList = new LinkedList<String>();
	public  static HashMap<String, Fragment> mBranchFragmentMap = new HashMap<String, Fragment>();
	
	/** 是否根Fragment*/
	private boolean mIsRoot;
	
	/**
	 * 显示根Fragment
	 * @param fMana
	 * @param contentId
	 * @param tag
	 */
	public void showRoot(FragmentManager fMana, int contentId, String tag){
		if(TextUtils.isEmpty(tag)){
			new Throwable(" the 'tag'=null do not allowed ");
			return;
		}
		
		if(!mBranchTagList.isEmpty() && tag.equals(mRootTagList.get(mRootTagList.size() - 1))){
			return;
		}
		
		removeAllBranch();
		mIsRoot = true;
		FragmentTransaction  transaction = fMana.beginTransaction();
		
		if(!mRootTagList.isEmpty()){
			transaction.hide(mRootFragmentMap.get(mRootTagList.get(mRootTagList.size() - 1)));
		}
		
		if(isAdded()){
			removeFragment(mIsRoot, tag);
			transaction.show(this);
		}else{
			transaction.add(contentId, this, tag);
		}
		addFragment(mIsRoot, this, tag);
		transaction.commitAllowingStateLoss();
	}
	
	/**
	 * 显示分支Fragment
	 * @param fMana
	 * @param contentId
	 * @param tag
	 * @param addToBackStack
	 */
	public void showBranch(FragmentManager fMana, int contentId, String tag, boolean addToBackStack){
		if(TextUtils.isEmpty(tag)){
			new Throwable(" the 'tag'=null do not allowed ");
			return;
		}
		
		mIsRoot = false;
		FragmentTransaction  transaction = fMana.beginTransaction();
		
		if(!mRootTagList.isEmpty()){
			transaction.hide(mRootFragmentMap.get(mRootTagList.get(mRootTagList.size() - 1)));
		}
		
		if(!mBranchTagList.isEmpty()){
			transaction.hide(mBranchFragmentMap.get(mBranchTagList.get(mBranchTagList.size() - 1)));
		}
		
		if(isAdded()){
			removeFragment(mIsRoot, tag);
			transaction.show(this);
		}else{
			transaction.add(contentId, this, tag);
		}
		addFragment(mIsRoot, this, tag);
		if(addToBackStack){
			transaction.addToBackStack(tag);
		}
		transaction.commitAllowingStateLoss();
	}
	
	/** 移除当前fragment*/
	public void remove(){
		if(!mIsRoot){
			getActivity().onBackPressed();
		}
	}
	
	/** 移除所有分支fragment*/
	public static void removeAllBranch(){
		if(mBranchTagList.isEmpty()){
			return;
		}
		
		for(String tag : mBranchTagList){
			Fragment frg = mBranchFragmentMap.get(tag);
			frg.getFragmentManager().beginTransaction().detach(frg).commit();
		}
		mBranchTagList.clear();
		mBranchFragmentMap.clear();
	}
	
	/** 添加一个Fragment*/
	private static void addFragment(boolean isRoot, Fragment frg, String tag){
		if(isRoot){
			mRootTagList.add(tag);
			mRootFragmentMap.put(tag, frg);
		}else{
			mBranchTagList.add(tag);
			mBranchFragmentMap.put(tag, frg);
		}
	}
	
	/** 移除一个Fragment*/
	private static void removeFragment(boolean isRoot, String tag){
		if(isRoot){
			if(mRootTagList.contains(tag)){
				mRootTagList.remove(tag);
				mRootFragmentMap.remove(tag);
			}
		}else{
			if(mBranchTagList.contains(tag)){
				mBranchTagList.remove(tag);
				mBranchFragmentMap.remove(tag);
			}
		}
	}
	
	/** 获取最顶端的Fragment*/
	public static Fragment getTopFragment(){
		Fragment rootTopFrg = topFragment(mRootFragmentMap, mRootTagList);
		if(rootTopFrg != null){
			return rootTopFrg;
		}
		
		Fragment branchTopFrg = topFragment(mBranchFragmentMap, mBranchTagList);
		if(branchTopFrg != null){
			return branchTopFrg;
		}
		
		return null;
	}
	
	private static Fragment topFragment(HashMap<String, Fragment> map, List<String> list){
		if(!list.isEmpty()){
			Fragment topFrg = map.get(list.get(list.size() - 1));
			if(!topFrg.isHidden()){
				return topFrg;
			}
		}
		return null;
	}
	

	@Override
	public void onDestroy() {
		super.onDestroy();
		removeFragment(mIsRoot, getTag());
	}
	
	
	
	class FrgSet extends LinkedHashMap<String, Fragment>{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public FrgSet putFrg(String key, Fragment Value){
			put(key, Value);
			return this;
		}
	}
}
