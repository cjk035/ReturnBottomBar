package com.keier.fragment.demo;

import com.keier.fragment.R;
import com.keier.fragment.backstack.BackStackFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

/**
 * �㳡
 * @author keier
 *
 */
public class OneFragment extends BackStackFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.one_fragment, null);
		view.findViewById(R.id.one).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				TwoFragment twoFragment = new TwoFragment();
				twoFragment.showBranch(getFragmentManager(), R.id.content, "two", true);
			}
		});
		return view;
	}
}
