package com.keier.fragment.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import com.keier.fragment.R;

public class MainActivity extends FragmentActivity implements OnClickListener{

	private static final String TAB_TAG_HOME = "home";
	private static final String TAB_TAG_QUEUE = "queue";
	private static final String TAB_TAG_SQUARE = "square";
	private static final String TAB_TAG_MINE = "mine";
	
	public static final int TAB_INDEX_HOME = 1;
	public static final int TAB_INDEX_QUEUE = 2;
	public static final int TAB_INDEX_SQUARE = 3;
	public static final int TAB_INDEX_MINE = 4;
	
	private int mTabIndex;
	private HomeFragment homeFragment;
	private QueueFragment queueFragment;
	private SquareFragment squareFragment;
	private MineFragment mineFragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		try {
			homeFragment = HomeFragment.class.newInstance();
			queueFragment = QueueFragment.class.newInstance();
			squareFragment = SquareFragment.class.newInstance();
			mineFragment = MineFragment.class.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		initBottomTab();
	}
	
	private void initBottomTab(){
		findViewById(R.id.homeTab).setOnClickListener(this);
		findViewById(R.id.queueTab).setOnClickListener(this);
		findViewById(R.id.squareTab).setOnClickListener(this);
		findViewById(R.id.mineTab).setOnClickListener(this);
		HomeFragment homeFragment = new HomeFragment();
		homeFragment.showRoot(getSupportFragmentManager(), R.id.content, TAB_TAG_HOME);
	}
	
	private void showTab(int index){
		mTabIndex = index;
		switch (index) {
		case TAB_INDEX_HOME:
			homeFragment.showRoot(getSupportFragmentManager(), R.id.content, TAB_TAG_HOME);
			break;
		case TAB_INDEX_QUEUE:
			queueFragment.showRoot(getSupportFragmentManager(), R.id.content, TAB_TAG_QUEUE);	
			break;
		case TAB_INDEX_SQUARE:
			squareFragment.showRoot(getSupportFragmentManager(), R.id.content, TAB_TAG_SQUARE);
			break;
		case TAB_INDEX_MINE:
			mineFragment.showRoot(getSupportFragmentManager(), R.id.content, TAB_TAG_MINE);
			break;
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.homeTab:
			showTab(TAB_INDEX_HOME);
			break;
		case R.id.queueTab:
			showTab(TAB_INDEX_QUEUE);	
			break;
		case R.id.squareTab:
			showTab(TAB_INDEX_SQUARE);
			break;
		case R.id.mineTab:
			showTab(TAB_INDEX_MINE);
			break;
		}
	}
	
	public static void launch(Context ctx, Bundle bundle){
		Intent intent = new Intent(ctx, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		if (null != bundle) { // 传递用户点击notification传入的bundle
			intent.putExtras(bundle);
		}
		ctx.startActivity(intent);
	}
}
