package com.keier.fragment.demo;

import com.keier.fragment.R;
import com.keier.fragment.backstack.BackStackFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

/**
 * �㳡
 * @author keier
 *
 */
public class TwoFragment extends BackStackFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.two_fragment, null);
		view.findViewById(R.id.two).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				ThreeFragment threeFragment = new ThreeFragment();
				threeFragment.showBranch(getFragmentManager(), R.id.content, "three", true);
			}
		});
		return view;
	}
}
