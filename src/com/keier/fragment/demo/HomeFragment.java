package com.keier.fragment.demo;


import com.keier.fragment.R;
import com.keier.fragment.backstack.BackStackFragment;
import com.keier.fragment.demo.util.ReTurnOperator;
import com.keier.fragment.demo.util.ReTurnOperator.ReturnType;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
/**
 * ��ҳ
 * @author keier
 *
 */
public class HomeFragment extends BackStackFragment {
	
	private ListView mListView;
	private ReTurnOperator mReturnOperator;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.home_fragment, null);
		mListView = (ListView) view.findViewById(R.id.listview);
		View returnView = ((MainActivity)getActivity()).findViewById(R.id.bottomTab);
		mReturnOperator = new ReTurnOperator(mListView, returnView);
		return view;
	}
	
	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		String[] strArray = {"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF", 
				"AAAAAAAAAAAAAAAAA", "BBBBBBBBBBBBBBBB", "CCCCCCCCCCCCCC",
				"DDDDDDDDDDDDD", "EEEEEEEEEEEEEEE", "FFFFFFFFFFFF"};
		mListView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item, R.id.textView, strArray));
		mListView.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				mReturnOperator.reTurnOperate(ReturnType.RETURN_FOOTER);
			}
		});
	}
	
}
