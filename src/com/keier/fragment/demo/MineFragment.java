package com.keier.fragment.demo;

import com.keier.fragment.R;
import com.keier.fragment.backstack.BackStackFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

/**
 * ��
 * @author keier
 *
 */
public class MineFragment extends BackStackFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.mine_fragment, null);
		view.findViewById(R.id.mine).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				OneFragment oneFragment = new OneFragment();
				oneFragment.showBranch(getFragmentManager(), R.id.content, "one", true);
			}
		});
		return view;
	}
}
